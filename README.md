# PlayBill
PlayBill is a platform where people have the opportunity to quickly find a suitable job in the Republic of Armenia and apply for different positions at the same time with one click. PlayBill created with Node.js(Express), React and MongoDB.
## Installation
### Database
Need to install and run MongoDB
### Server
Install packages
```bash
npm install
```
Run Node.js
```bash
nodemon app
```
### Client
Install packages
```bash
npm install
```
Run React.js
```bash
npm start
```
