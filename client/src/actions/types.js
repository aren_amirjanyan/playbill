export const GET_ERRORS = 'GET_ERRORS';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const CREATE_USER = 'CREATE_USER';
export const GET_USERS = 'GET_USERS';
export const GET_USER = 'GET_USER';
export const EDIT_USER = 'EDIT_USER';
export const ADD_USER = 'ADD_USER';
export const DELETE_USERS = 'DELETE_USERS';
/**
 * Job action types
 */
export const GET_JOBS = 'GET_JOBS';

export const GET_JOBS_REQUEST = 'GET_JOBS_REQUEST';
export const GET_JOBS_SUCCESS = 'GET_JOBS_SUCCESS';
export const GET_ERRORS_FAILURE = 'GET_ERRORS_FAILURE';

export const GET_JOB = 'GET_JOB';
export const EDIT_JOB = 'EDIT_JOB';
export const ADD_JOB = 'ADD_JOB';
export const DELETE_JOBS = 'DELETE_JOBS';